# Windowz

- need to build release on appveyor ci
- need to build installer that will install the redist dlls for ms
   vcreddist_x64.exe <---
- need to build installer that will unzip the release
- need to runn the build/appname install script

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `windowz` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:windowz, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/windowz](https://hexdocs.pm/windowz).

