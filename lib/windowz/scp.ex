defmodule Windowz.Scp do
  use GenServer

  require Logger

  def init(_args) do
    port = Port.open({:spawn_executable, medkitscp_path()}, args: medkitscp_args())
    Port.monitor(port)
    {:ok, port}
  end

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def alive? do
    GenServer.call(__MODULE__, :alive)
  end

  def handle_message(name, data) do
    :ok = Logger.info("#{inspect(name)} #{inspect(data)}")
  end

  def handle_info({:DOWN, _ref, :port, _port, :normal}, port) do
    {:stop, :normal, port}
  end

  def handle_info({_port, {:data, message}}, port) do
    case Jason.decode(message) do
      {:ok, %{"name" => name, "data" => data}} ->
        :ok = handle_message(name, data)
        {:noreply, port}

      {:error, error} ->
        :ok = Logger.error(inspect(error))
        {:noreply, port}
    end
  end

  def handle_call(:alive, _, port) do
    info = Port.info(port)
    {:reply, {:ok, info != nil}, port}
  end

  def medkitscp_path do
    Path.join([
      :code.priv_dir(:windowz),
      "bin",
      "windows",
      "scp.exe"
    ])
  end

  def medkitscp_args do
    ["11111", "C:\\"]
  end
end
