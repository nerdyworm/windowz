defmodule Windowz.Worker do
  require Logger
  use GenServer

  def start_link(_) do
    GenServer.start_link(__MODULE__, [])
  end

  def init(stack) do
    send(self(), :loop)
    {:ok, stack}
  end

  @impl true
  def handle_call(:pop, _from, [head | tail]) do
    {:reply, head, tail}
  end

  @impl true
  def handle_cast({:push, item}, state) do
    {:noreply, [item | state]}
  end

  def handle_info(:loop, state) do
    Logger.info("==================  Info ===================================")
    Logger.info("version: #{inspect(System.version())}")
    Logger.info("cwd: #{inspect(System.cwd())}")
    Logger.info("arg: #{inspect(System.argv())}")
    Logger.info("tmp: #{inspect(System.tmp_dir())}")
    Logger.info("home: #{inspect(System.user_home())}")
    Logger.info(inspect(:code.priv_dir(:windowz)))
    Logger.info("env: #{inspect(System.get_env(), pretty: true, limit: 3000)}")
    Logger.info("==================  Info END ===================================")
    Process.send_after(self(), :loop, 30_000)
    {:noreply, state}
  end
end
