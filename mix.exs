defmodule Windowz.MixProject do
  use Mix.Project

  def project do
    [
      app: :windowz,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Windowz.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:logger_file_backend, "~> 0.0.10"},
      {:distillery, git: "https://github.com/bitwalker/distillery"},
      {:jason, "~> 1.0.0"}
    ]
  end
end
