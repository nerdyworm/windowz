FROM dockcross/windows-x86
# FROM dockcross/windows-x64
# FROM ubuntu:16.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install wget
RUN wget http://erlang.org/download/otp_src_21.0.tar.gz
RUN tar -xvf otp_src_21.0.tar.gz

RUN apt-get install -y m4
RUN apt-get install -y libssl-dev
RUN apt-get install -y libncurses5-dev

WORKDIR  ./otp_src_21.0

RUN ./configure --host=i686 --build=i686-w64-mingw32
RUN make
# RUN apt-get install -y \
#   gcc-mingw-w64-i686 \
#   gcc-mingw-w64-x86-64 \
#   gcc-multilib \
#   m4 \
#   xorg \
#   xorg-dev \
#   libx11-dev

# RUN apt-get install -y \
#   libpcre3-dev \
#   libssl-dev

# RUN apt-get install -y \
#   ocaml \
#   opam


